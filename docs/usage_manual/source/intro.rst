######################
Introduction
######################

What is Chromite 
----------------

Chromite is the commerically supported version of the open source `SHAKTI <https://shakti.org.in>`_
C-class processor. It is an extremely configurable and commercial-grade 5-stage in-order core 
supporting the standard RV64GCSUN ISA extensions. The core generator in this repository is capable 
of configuring the core to generate a wide variety of design instances from the same high-level 
source code. The design instances can serve domains ranging from embedded systems, motor-control, 
IoT, storage, industrial applications all the way to low-cost high-performance linux based 
applications such as networking, gateways etc. The extreme parameterization
of the design in conjunction with using an HLS like Bluespec, it makes it easy to add new features
and design points on a continual basis.

License
-------
All of the source code available in this repository is under the BSD license. 
Please refer to LICENSE.* files for more details.

