#######################
Chromite Core Generator
#######################

.. .. raw:: latex
.. 
..   \begin{figure*}
..     \centering
..     \includegraphics[scale=.55]{../../source/_static/ChromiteSystem.png}
..     \caption{Chromite Processing System}
..   \end{figure*}

.. toctree::
  :glob:
  :maxdepth: 2
  :numbered:

  overview
  feature
  pipeline
  modes
  interrupt
  memory
  micro-arch-notes
  businterface
  plic
  debugsupport
  benchmarking
  software
  options
  revisions

.. bibliography:: refs.bib
  :all:
