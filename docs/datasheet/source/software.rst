Software Ecosystem
==================

Since the Chromite core is based on the RISC-V ISA, the users can leverage the entire open source
RISC-V toolchain which includes the following:

* Object toolchain:

  * Binutils
  * LLVM
  * Cranelift

* Debugging:

  * GDB
  * OpenOCD
  * Platform IO IDE
 
* Compilers and Libraries

  * GCC
  * Clang/LLVM
  * Glibc
  * Newlib
 
* Bootloaders:

  * U-Boot
  * Coreboot
  * Open-SBI
 
* OS and OS Kernels:

  * Linux
  * FreeRTOS
  * Zephyr RTOS
  * NuttX
  * Sel4
 
* Compilers and runtimes for other languages:

  * Go
  * OCaml
  * Rust
 
* Machine Learning/AI

  * TensorFlow Lite
